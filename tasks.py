from invoke import task
import gitlab_api_interface

@task
def brs(c):
    """
    Show branches of current repository.
    Include:
            branches name
            total branches number
    """
    
    branches_list = gitlab_api_interface.get_branches()
    print(f"There are {len(branches_list)} branches on tasks repository:")
    for branch in branches_list:
       print(f"        {branch}")

    

@task
def ver(c):
    """
    echo PoL version
    """
    
    print("PoL version: ")

@task
def iss(c):
    """
    Issue scan
    """

    issues_list = gitlab_api_interface.get_issues()
    print(f"There are {len(issues_list)} issues on tasks repository:")
    for issue in issues_list:
        print(f"issue_id:{issue['iid']} title:{issue['title']} creator:{issue['username']} notes:{len(issue['notes'])}")

@task
def pol(c):
    """
    prove for learning
    """

    gitlab_api_interface.pol()