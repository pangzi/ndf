import requests, sys, json, datetime
from pprint import pprint
import matplotlib.pyplot as plt
import numpy as np
import time
import os

# get gitlab api access token from ci/cd variables
gitlab_access_token = os.environ['GITLAB_API_TOKEN']
#------------------------------------------------------------
#                         ch1
#------------------------------------------------------------
def get_branches():
    """
    get branches info through gitlab-api
    project id: 16345223
    return: a branches list
    """
    BRANCHES_URL = "https://gitlab.com/api/v4/projects/16345223/repository/branches"
    parameters = {
        "private_token": gitlab_access_token,
        "per_page": 100
    }

    response = requests.get(BRANCHES_URL, params=parameters)

    return [branch["name"] for branch in response.json()]
#------------------------------------------------------------
#                         ch2
#------------------------------------------------------------
def get_notes(issue_id):
    """
    get notes of an issue through gitlab-api
    return: a notes list of given issue
    """
    notes_dict = []

    ISSUE_NOTES_URL = f"https://gitlab.com/api/v4/projects/16345223/issues/{str(issue_id)}/notes"
    parameters = {
        "private_token": gitlab_access_token,
        "per_page": 100,
        "page": 1
        }
    
    while True:
        response = requests.get(ISSUE_NOTES_URL, params=parameters)
        
        for note in response.json():
                if note["system"] == False:
                    note_json = {}
                    note_json["username"] = note["author"]["username"]
                    note_json["created_at"] = note["created_at"]
                    notes_dict.append(note_json)

        if parameters['page'] == int(response.headers['X-Total-Pages']):
            break

        parameters['page'] += 1

    return notes_dict


def get_issues():
    """
    get issues info through gitlab-api
    return: an issues list
    """
    issues_list = []

    ISSUES_URL = "https://gitlab.com/api/v4/projects/16345223/issues"
    parameters = {
        "private_token": gitlab_access_token,
        "scope": "all",
        "state": "all",
        "per_page": 100,
        "page": 1
    }
    while True:
        response = requests.get(ISSUES_URL, params=parameters)

        for i in range(len(response.json())):
            
            issue_dict = {}
            issue_dict["iid"] = response.json()[i]["iid"]
            issue_dict["username"] = response.json()[i]["author"]["username"]
            issue_dict["created_at"] = response.json()[i]["created_at"]
            issue_dict["title"] = response.json()[i]["title"]
            issue_dict["notes"] = get_notes(issue_dict["iid"])        

            issues_list.append(issue_dict)

        if parameters['page'] == int(response.headers['X-Total-Pages']):
            break

        parameters['page'] += 1

    # for test
    # pprint(issues_list)
    notes = []
    for issue in issues_list:
        for note in issue['notes']:
            notes.append(note)
    print(f'{len(issues_list)} issues and {len(notes)} notes.')

    with open('issues.json', 'w') as f:
        json.dump(issues_list, f)

    return issues_list
#------------------------------------------------------------
#                         ch3
#------------------------------------------------------------
def get_commits():
    """
    Get commits of branches, store to commits_list, save to commits.json.
    commits_list = [commit_dict1, commit_dict2, commit_dict3, ...]
    commit_dict = {
                    "id": "502b6a15537dd06afdcbc104bfffb1972c48ead8",
                    "author_name": "bruceq619",
                    "created_at": "2020-03-18T09:47:50.000+08:00",
                    "title": "change gitlab access token",
                    "content": {
                                "additions": 29
                                "deletions": 3,
                                "total": 32,
                                "change_files": 4
                                }
                    "comments: [
                                    {
                                    "name": "bruceq619",
                                    "created_at": "2020-03-18T09:47:50.000+08:00",
                                    "contents": comment_contents
                                    },
                                    {
                                        ...
                                    }
                                ]
                  }
    """
    commits_list = []
    
    COMMITS_URL = "https://gitlab.com/api/v4/projects/16345223/repository/commits"
    
    branches_list = get_branches()
    for branch in branches_list:

        parameters = {
            "private_token": gitlab_access_token,
            "per_page": 100,
            "ref_name": branch,
            "page": 1
            }
        while True:
            response = requests.get(COMMITS_URL, params=parameters)

            for i in range(len(response.json())):
                commit_dict = {}
                commit_dict["id"] = response.json()[i]["id"]
                commit_dict["author_name"] = response.json()[i]["author_name"]
                commit_dict["created_at"] = response.json()[i]["created_at"]
                commit_dict["title"] = response.json()[i]["title"]
                # commit_dict["content"] = get_commit_contents(response.json()[i]["id"])
                commit_dict["content"] = {}
                commit_dict["comments"] = get_commit_comments(response.json()[i]["id"])

                commits_list.append(commit_dict)

            if parameters['page'] == int(response.headers['X-Total-Pages']):
                break

            parameters['page'] += 1

    # for test
    comments = []
    for commit in commits_list:
        for comment in commit['comments']:
            comments.append(comment)
    print(f'{len(commits_list)} commits and {len(comments)} comments.')

    # write to json file
    with open('commits.json', 'w') as f:
        json.dump(commits_list, f)

    return commits_list

# def commit_diff_line(additions, deletions):
#     """
#     return add/delete/edit lines number from additions and deletions number
#     """
#     if additions > deletions:
#         add = additions - deletions
#         edit = deletions
#         delete = 0
#     elif additions < deletions:
#         delete = deletions - additions
#         edit = additions
#         add = 0
#     else:
#         edit = additions
#         add = 0
#         delete = 0

#     return add, delete, edit

def get_change_files_number(id):
    """
    Get change files number from commit diff api.
    """
    file_list = []

    COMMIT_DIFF_URL = f"https://gitlab.com/api/v4/projects/16345223/repository/commits/{id}/diff"

    parameters = {"private_token": gitlab_access_token,
                  "per_page": 100,
                  "page": 1
                  }

    while True:
        response = requests.get(COMMIT_DIFF_URL, params=parameters)

        for item in response.json():
            file_list.append(item)

        if parameters['page'] == int(response.headers['X-Total-Pages']):
            break

        parameters['page'] += 1

    return len(file_list)

def get_commit_contents(id):
    """
    Get commit contents with commit id, return a commit_contents dict.
    param: id
    commit_contents = {
                       "additions": 29
                       "deletions": 3,
                       "total": 32,
                       "change_files": 4
                       }
    """
    COMMIT_URL = f"https://gitlab.com/api/v4/projects/16345223/repository/commits/{id}"
    parameters = {"private_token": gitlab_access_token}
    response = requests.get(COMMIT_URL, params=parameters)

    commit_contents = {}
    commit_contents["additions"] = response.json()["stats"]["additions"]
    commit_contents["deletions"] = response.json()["stats"]["deletions"]
    commit_contents["total"] = response.json()["stats"]["total"]
    commit_contents["change_files"] = get_change_files_number(id)

    return commit_contents
    

def get_commit_comments(id):
    """
    Get commit comments with commit id, return a commit_comments list.
    param: id
    commit_comments_list = [commit_comment1, commit_comment2, commit_comment3, ...]
    commit_comment = {
                       "name": "bruceq619",
                       "created_at": "2020-03-18T09:47:50.000+08:00",
                       "contents": comment_contents
                      }
    """
    commit_comments_list = []

    COMMIT_COMMENT_URL = f"https://gitlab.com/api/v4/projects/16345223/repository/commits/{id}/comments"
    parameters = {"private_token": gitlab_access_token,
                  "per_page": 100,
                  "page": 1
                  }
    while True:

        response = requests.get(COMMIT_COMMENT_URL, params=parameters)
        
        for i in range(len(response.json())):
            commit_comment = {}
            commit_comment["name"] = response.json()[i]["author"]["username"]
            commit_comment["created_at"] = response.json()[i]["created_at"]
            commit_comment["contents"] = {}
            commit_comments_list.append(commit_comment)

        if parameters['page'] == int(response.headers['X-Total-Pages']):
            break

        parameters['page'] += 1

    return commit_comments_list

#------------------------------------------------------------
#                         pol output
#------------------------------------------------------------

# 101camp start day
Day_One_object = datetime.datetime.strptime("2020-02-23", "%Y-%m-%d")

# list week days by week no.
def week_days(week_no):
    week_days_list = []
    week_day_one = Day_One_object + datetime.timedelta(days=week_no*7)
    for i in range(7):
        week_days_list.append((week_day_one + datetime.timedelta(days=i)).strftime("%Y-%m-%d"))
    return week_days_list

# count commit number
def count_commit_number(json_data, name, period):
    filter_list = filter(lambda x:x['author_name']==name and x['created_at'][:10] in period, json_data)
    return len(list(filter_list))

# count commit comment number
def count_comment_number(json_data, name, period):
    filter_list = filter(lambda x:x['name']==name and x['created_at'][:10] in period, json_data)
    return len(list(filter_list))

# count ci number by week no.
def count_ci_by_week(name, week_no):
    week_days_list = week_days(week_no)
    return count_commit_number(commits, name, week_days_list)

# count cc number by week no.
def count_cc_by_week(name, week_no):
    comments_list = []
    for commit in commits:
        for comment in commit["comments"]:
            comments_list.append(comment)
    week_days_list = week_days(week_no)
    return count_comment_number(comments_list, name, week_days_list)

# count issue & note number
def count_issue_number(json_data, name, period):
    filter_list = filter(lambda x:x['username']==name and x['created_at'][:10] in period, json_data)
    return len(list(filter_list))

# count is number by week no.
def count_is_by_week(name, week_no):
    week_days_list = week_days(week_no)
    return count_issue_number(issues, name, week_days_list)

# count ic number by week no.
def count_ic_by_week(name, week_no):
    notes_list = []
    for issue in issues:
        for note in issue['notes']:
            notes_list.append(note)
    week_days_list = week_days(week_no)
    return count_issue_number(notes_list, name, week_days_list) 

# collect all user name in project
def get_users():
    users_list = []
    for commit in commits:
        if commit['author_name'] not in users_list:
            users_list.append(commit['author_name'])
        for comment in commit['comments']:
            if comment['name'] not in users_list:
                users_list.append(comment['name'])
    for issue in issues:
        if issue['username'] not in users_list:
            users_list.append(issue['username'])
        for note in issue['notes']:
            if note['username'] not in users_list:
                users_list.append(note['username'])

    return list(filter(lambda x:x not in ['ZoomQuiet', 'Zoom.Quiet'], users_list))

# collect weekly proof of work by user
def pol(weeks):
    pol_json = {}
    for user in get_users():
        pol_json[user] = {}
        pol_json[user]['sum'] = {}
        pol_json[user]['sum']['ci'] = 0
        pol_json[user]['sum']['cc'] = 0
        pol_json[user]['sum']['is'] = 0
        pol_json[user]['sum']['ic'] = 0
        for w in range(weeks):
            pol_json[user][f'w{w}'] = {}
            pol_json[user][f'w{w}']['ci'] = count_ci_by_week(user, w)
            pol_json[user][f'w{w}']['cc'] = count_cc_by_week(user, w)
            pol_json[user][f'w{w}']['is'] = count_is_by_week(user, w)
            pol_json[user][f'w{w}']['ic'] = count_ic_by_week(user, w)
            pol_json[user]['sum']['ci'] += pol_json[user][f'w{w}']['ci']
            pol_json[user]['sum']['cc'] += pol_json[user][f'w{w}']['cc']
            pol_json[user]['sum']['is'] += pol_json[user][f'w{w}']['is']
            pol_json[user]['sum']['ic'] += pol_json[user][f'w{w}']['ic']
        
    return pol_json

# list last 7 days from today
def last_7_days():
    last_7_days = []
    today = datetime.date.today()
    for i in range(7):
        last_7_days.append((today - datetime.timedelta(days=i+1)).strftime("%Y-%m-%d"))
    return last_7_days

# collect proof of work by user last 7 day
def pol_7days():
    pol_json = {}
    for user in get_users():
        pol_json[user] = {}
        pol_json[user]['sum'] = {}
        pol_json[user]['sum']['ci'] = count_commit_number(commits, user, last_7_days())
        comments_list = []
        for commit in commits:
            for comment in commit["comments"]:
                comments_list.append(comment)
        pol_json[user]['sum']['cc'] = count_comment_number(comments_list, user, last_7_days())
        pol_json[user]['sum']['is'] = count_issue_number(issues, user, last_7_days())
        notes_list = []
        for issue in issues:
            for note in issue['notes']:
                notes_list.append(note)
        pol_json[user]['sum']['ic'] = count_issue_number(notes_list, user, last_7_days())
    
    return pol_json

# sort proof of work by kpi
def sort_pol(pol_json, kpi):
    return sorted(pol_json.items(), key=lambda x: x[1]['sum'][kpi], reverse=True)[:5]

#------------------------------------------------------------
#                         plot and save to file
#------------------------------------------------------------
# plot horizontal bar
def hbar(output_list, period, kpi):
    with plt.xkcd():
        fig = plt.figure()
        ax = fig.add_axes((0.18, 0.2, 0.8, 0.7))
        ax.spines['right'].set_color('none')
        ax.spines['top'].set_color('none')

        #  data
        people = [ x[0] for x in output_list]
        y_pos = np.arange(len(people))
        performance = [ x[1]['sum'][kpi] for x in output_list]

        ax.barh(y_pos, performance, height=0.25, align='center')
        ax.set_yticks(y_pos)
        ax.set_yticklabels(people, rotation=45)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_xlabel('Numbers')
        ax.set_title(f'Top 5 {kpi}')
        for index, value in enumerate(performance):
            plt.text(value, index, str(value))

    # plt.show()
    plt.savefig(f'public/images/top5_{period}_{kpi}.png')




if __name__ == "__main__":

    # test runtime
    # start_time = time.time()

    # 1. get issues & issue notes from gitlab api and save to issues.json
    issues = get_issues()

    # 2. get commits & commit comments from gitlab api and save to commits.json
    commits = get_commits()

    # 3. open commits.json & issues.json file
    # with open('commits.json') as f:
    #     commits = json.load(f)
    # with open('issues.json') as f:
    #     issues = json.load(f)

    # 4. generate statistic data from issues.json & commits.json
    # pprint(pol(5))
    # pprint(pol_7days())

    # 5. generate statistic chart
    hbar(sort_pol(pol(5), 'ci'), 'total', 'ci')
    hbar(sort_pol(pol(5), 'cc'), 'total', 'cc')
    hbar(sort_pol(pol(5), 'is'), 'total', 'is')
    hbar(sort_pol(pol(5), 'ic'), 'total', 'ic')
    hbar(sort_pol(pol_7days(), 'ci'), '7d', 'ci')
    hbar(sort_pol(pol_7days(), 'cc'), '7d', 'cc')
    hbar(sort_pol(pol_7days(), 'is'), '7d', 'is')
    hbar(sort_pol(pol_7days(), 'ic'), '7d', 'ic')

    # test runtime
    # run_time = int((time.time() - start_time) / 60)
    # print(f'Run for {run_time} minutes.')