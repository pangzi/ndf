CH4 task 展示 by DoP@5py team

[TOC]

## 任务步骤

- step 1 数据图形展示
- step 2 gitlab-Pages
- step 3 CI/CD
- step 4 crontab 计划任务



## step1 数据图形展示

- 获取 Top5 数据  
略。。。
- 生成数据统计图片代码  
```python
def hbar(output_list, period, kpi):
    with plt.xkcd():
        fig = plt.figure()
        ax = fig.add_axes((0.18, 0.2, 0.8, 0.7))
        ax.spines['right'].set_color('none')
        ax.spines['top'].set_color('none')

        #  data
        people = [ x[0] for x in output_list]
        y_pos = np.arange(len(people))
        performance = [ x[1]['sum'][kpi] for x in output_list]

        ax.barh(y_pos, performance, height=0.25, align='center')
        ax.set_yticks(y_pos)
        ax.set_yticklabels(people, rotation=45)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_xlabel('Numbers')
        ax.set_title(f'Top 5 {kpi}')
        for index, value in enumerate(performance):
            plt.text(value, index, str(value))

    # plt.show()
    plt.savefig(f'public/images/top5_{period}_{kpi}.png')  
```
## step 2 gitlab-Pages
- fork 大妈的 Demo
- 修改 HTML 文件，图片位置代码如下：
```html
<p>
      <img src="images/top5_total_ci.png" alt="top5_ci"><img src="images/top5_total_cc.png" alt="top5_cc"><img src="images/top5_total_is.png" alt="top5_is"><img src="images/top5_total_ic.png" alt="top5_ic">
    </p>
```
- 图片放入`public/image`目录下
- 以上git push即可刷新页面 <https://bruceq619.gitlab.io/demo>

## step 3 CI/CD
- .gitlab-ci.yml 配置
```yml
image: alpine:latest
image: python:3.7.3

before_script:
  - python -V
  

run:
  script:
    - pip install -r requirements.txt
    - python gitlab_api_interface.py
  rules:
    - exists:
      - deploy/_deploy.md
  artifacts:
    paths:
      - public

pages:
  stage: deploy
  script:
  - apt-get update -qq && apt-get install -qq -y pandoc
  - pandoc --version
  - pandoc log/ch4.md -o public/ch4.html -t revealjs -s -V theme=beige
  artifacts:
    paths:
    - public
  only:
  - master
```
- 本地仓库 git 操作

- 触发 Pipelines 
![img](https://tva1.sinaimg.cn/large/00831rSTly1gda5xonja4j31rz0tm42s.jpg)

- 查看运行日志  
点击` job`可看到具体失败或成功的提示
![img](https://tva1.sinaimg.cn/large/00831rSTly1gda6aw7n37j31np0u0woj.jpg)
- 查看 Analytics
![img](https://tva1.sinaimg.cn/large/00831rSTly1gda647ndjhj31xq0tcn2s.jpg)



## step 4 计划任务
- crontab
> 在本机配定时运行的计划任务，因为时间关系没有调试成功
![img](https://tva1.sinaimg.cn/large/00831rSTly1gda6fkpqmej31tg04udhj.jpg)
- Scheduling Pipelines （没测试过）
>The pipelines schedule runs pipelines in the future, repeatedly, for specific branches or tags. Those scheduled pipelines will inherit limited project access based on their associated user.
Learn more in the pipeline schedules documentation.

## 附加 runner 部分
> runner 包括了 Specific Runners & Shared Runners，前者需要安装在本机。
- step 1 下载 & 安装
略。。。 
- step 2 runner 注册，关键信息  
- token
- Tags
```
sudo gilab-runner register
Password:
sudo: gilab-runner: command not found
localhost:~ qinbo$ sudo gitlab-runner register
Runtime platform                                    arch=amd64 os=darwin pid=33814 revision=4c96e5ad version=12.9.0
Running in system-mode.

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com/
Please enter the gitlab-ci token for this runner:
huy5xamVSmt8epyyBA5K
Please enter the gitlab-ci description for this runner:
[192.168.0.110]: dboder_runner
Please enter the gitlab-ci tags for this runner (comma separated):
dboder_runner
Registering runner... succeeded                     runner=huy5xamV
Please enter the executor: ssh, virtualbox, docker-ssh+machine, kubernetes, custom, docker, docker-ssh, parallels, shell, docker+machine:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!  
```
- 与Runners 配置里的 Tags 对应上

![Tags](https://tva1.sinaimg.cn/large/00831rSTly1gd9mwel3uaj310d0p2q66.jpg)

### step 2
![CI 中 Stage 的执行](https://tva1.sinaimg.cn/large/00831rSTly1gd9n9xshwsj30zy0hngnp.jpg)  
以图中所示为例，整个 CI 环节包含三个 Stage：build、test 和
deploy。  
- build 被首先执行。如果发生错误，本次 CI 立刻失败；  
- test 在 build 成功执行完毕后执行。如果发生错误，本次 CI 立刻失败；  
- deploy 在 test 成功执行完毕后执行。如果发生错误，本次 CI 失败。  
Stage 在 .gitlab-ci.yml 中通过如下的方式定义：  
```
stages:
  - build
  - test
  - deploy
```
- 如果文件中没有定义 stages，那么则默认包含 build、test 和 deploy 三个 stage。  

## rules:exists  
```
job:
  script: docker build -t my-image:$CI_COMMIT_REF_SLUG .
  rules:
    - exists:
      - Dockerfile
```



## ch4 的总结  
- ch4 的 3 个任务均已完成
- 本周进行了 3 次 zoom 会议，充分沟通了彼此的认知和分工，均已录像。
- CI/CD 是完整的开发/测试/构建/部署/发布的自动化过程，可详细阅读官网材料
- Gitlab pipeline 指 一组按照 stage 执行的job(每个 stage 包含若干个 job)
- 当一个 stage 的 job 都成功执行后，开始执行下一个 stage 的 job。
- pipeline 定义在项目的 .gitlab-ci.yml里。关于 .gitlab-ci.yml 的详细参考文档，请见：<Configuration of your jobs with .gitlab-ci.yml>
- runner docker 方式是为了分别在`builder` 和`deploy`的不同 stage 里使用不同的运行环境

## ref  
- [GitLab CI/CD 的执行流程](https://www.jianshu.com/p/306cf4c6789a)
- [GitLab Continuous Integration (GitLab CI/CD)](https://link.jianshu.com/?t=https%3A%2F%2Fdocs.gitlab.com%2Fce%2Fci%2FREADME.html)  
- [GitLab: Configuration of your jobs with .gitlab-ci.yml](https://link.jianshu.com/?t=https%3A%2F%2Fdocs.gitlab.com%2Fce%2Fci%2Fyaml%2FREADME.html%23configuration-of-your-jobs-with-gitlab-ci-yml)  
